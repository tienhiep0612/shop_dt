<footer class="footer">

   <section class="grid">

      <div class="box">
         <h3>quick links</h3>
         <a href="home.php"> <i class="fas fa-angle-right"></i> Trang chủ</a>
         <a href="about.php"> <i class="fas fa-angle-right"></i> Giới thiệu</a>
         <a href="shop.php"> <i class="fas fa-angle-right"></i> Sản phẩm</a>
         <a href="contact.php"> <i class="fas fa-angle-right"></i> Liên hệ</a>
      </div>

      <div class="box">
         <h3>extra links</h3>
         <a href="user_login.php"> <i class="fas fa-angle-right"></i> Đăng nhập</a>
         <a href="user_register.php"> <i class="fas fa-angle-right"></i> Đăng ký</a>
         <a href="cart.php"> <i class="fas fa-angle-right"></i> Giỏ hàng</a>
         <a href="orders.php"> <i class="fas fa-angle-right"></i> Đơn hàng</a>
      </div>

      <div class="box">
         <h3>contact us</h3>
         <a href="#"><i class="fas fa-phone"></i> 0353827278</a>
         <a href="#"><i class="fas fa-phone"></i> 0982651026</a>
         <a href="mailto:nguyenquibac412@gmail.com"><i class="fas fa-envelope"></i> nguyenquibac412@gmail.com</a>
         <a href="#"><i class="fas fa-map-marker-alt"></i> 54 Triều Khúc, Thanh Xuân, Hà Nội </a>
      </div>

      <div class="box" style="margin-left: 80px;">
         <h3>follow us</h3>
         <a href="#"><i class="fab fa-facebook-f"></i>Facebook</a>
         <a href="#"><i class="fab fa-twitter"></i>Twitter</a>
         <a href="#"><i class="fab fa-instagram"></i>Instagram</a>
         <a href="#"><i class="fab fa-linkedin"></i>Linkedin</a>
      </div>

   </section>

</footer>